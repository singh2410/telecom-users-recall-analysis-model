#!/usr/bin/env python
# coding: utf-8

# # Telecom Users Recall Analysis
# #By- Aarush Kumar
# #Dated: June 23,2021

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


# In[2]:


data=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Telecom Users Recall Analysis/telecom_users.csv')


# In[3]:


data


# In[4]:


data.head()


# In[5]:


data.info()


# In[6]:


data.isnull().sum()


# In[7]:


data.shape


# In[8]:


data.size


# In[9]:


data.describe()


# In[10]:


# Convert TotalCharges to numeric type
data['TotalCharges'] = pd.to_numeric(data['TotalCharges'], errors='coerce')


# In[11]:


# check for imbalance in data
print(data['Churn'].value_counts())
plt.pie(data['Churn'].value_counts(), autopct='%1.1f%%', labels = ['No', 'Yes']);


# In[12]:


def plot_category(feature, figsize=None):
    yes_count = data[data['Churn']=='Yes'].groupby([feature]).size()
    no_count = data[data['Churn']=='No'].groupby([feature]).size()
    labels = no_count.index
    x = np.arange(len(labels)) # the label locations
    width = 0.35  # the width of the bars
    if figsize:
        fig, ax = plt.subplots(figsize=figsize)
    else:
        fig, ax = plt.subplots()
    rects1 = ax.bar(x-width/2, round(yes_count*100/data.groupby([feature]).size(), 2), 
                    width, label='Yes')
    rects2 = ax.bar(x+width/2, round(no_count*100/data.groupby([feature]).size(), 2), 
                    width, label='No')
    ax.set_ylabel('Count')
    ax.set_title('Based on %s'%feature)
    ax.set_xticks(x)
    ax.set_xticklabels(labels, rotation=80)
    ax.legend();
    ax.bar_label(rects1, padding=1)
    ax.bar_label(rects2, padding=1)
    fig.tight_layout()
    plt.show()


# In[13]:


def plot_numerical(feature, figsize=None):
    # Attrition vs Age Distribution
    fig = plt.figure(figsize=(10,6))
    sns.kdeplot(data[data['Churn']=='No'][feature])
    sns.kdeplot(data[data['Churn']=='Yes'][feature])
    fig.legend(labels=['Churn No', 'Churn Yes'])
    plt.title('Based on %s'%feature)
    plt.show()


# In[14]:


for feature in ['tenure']:
    plot_numerical(feature)


# In[16]:


for feature in ['gender', 'SeniorCitizen', 'Partner', 'Dependents']:
    plot_category(feature)


# In[18]:


for feature in ['SeniorCitizen']:
    plot_category(feature)


# In[19]:


for feature in ['Partner']:
    plot_category(feature)


# In[20]:


for feature in ['Dependents']:
    plot_category(feature)


# In[17]:


for feature in ['PhoneService', 'MultipleLines']:
    plot_category(feature)


# In[21]:


for feature in ['MultipleLines']:
    plot_category(feature)


# In[22]:


for feature in ['InternetService', 'OnlineSecurity', 'OnlineBackup', 'DeviceProtection', 
                'TechSupport', 'StreamingTV', 'StreamingMovies']:
    plot_category(feature)


# In[23]:


for feature in ['MonthlyCharges', 'TotalCharges']:
    plot_numerical(feature)


# In[24]:


categorical_features = ['gender', 'SeniorCitizen', 'Partner',
                       'Dependents', 'PhoneService', 'MultipleLines',
                       'InternetService', 'OnlineSecurity', 'OnlineBackup', 'DeviceProtection',
                       'TechSupport', 'StreamingTV', 'StreamingMovies', 'Contract',
                       'PaperlessBilling', 'PaymentMethod']
numerical_features = ['MonthlyCharges', 'TotalCharges', 'tenure']
to_drop = ['customerID', 'Unnamed: 0'] # contain all unique values or not relevant


# In[25]:


from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import os
import joblib


# In[26]:


df = data.copy()
path = '/home/aarush100616/Downloads/Projects/Telecom Users Recall Analysis'


# In[27]:


for i, feature in enumerate(categorical_features):
    le = LabelEncoder()
    # create directory to save label encoding models
    if not os.path.exists(os.path.join(path, "TextEncoding")):
        os.makedirs(os.path.join(path, "TextEncoding"))
    # perform label encoding
    le.fit(df[feature])
    # save the encoder
    joblib.dump(le, open(os.path.join(path, "TextEncoding/le_{}.sav".format(feature)), 'wb'))
    # transfrom training data
    df[feature] = le.transform(df[feature])
    # get classes & remove first column to elude from dummy variable trap
    columns = list(map(lambda x: feature+' '+str(x), list(le.classes_)))[1:]
     # save classes
    joblib.dump(columns, 
                open(os.path.join(path, "TextEncoding/le_{}_classes.sav".format(feature)), 'wb'))


# In[29]:


# Bivariate Analysis Correlation plot with the Numeric variables
plt.figure(figsize=(15,15))
sns.heatmap(round(data[numerical_features].corr(method='spearman'), 2), 
            annot=True, mask=None, cmap='GnBu')
plt.show()


# In[30]:


# Bivariate Analysis Correlation plot with the Categorical variables
plt.figure(figsize=(15, 15))
sns.heatmap(round(df[categorical_features+numerical_features].corr(method='spearman'), 2), annot=True,
            mask=None, cmap='GnBu')
plt.show()


# In[31]:


from statsmodels.stats.outliers_influence import variance_inflation_factor


# In[32]:


# Calculating VIF
vif = pd.DataFrame()
temp = df.dropna()
vif["variables"] = [feature for feature in categorical_features+numerical_features if feature not in ['MonthlyCharges', 'tenure']]
vif["VIF"] = [variance_inflation_factor(temp[vif['variables']].values, i) for i in range(len(vif["variables"]))]
print(vif)


# In[33]:


missingValueFeatures = pd.DataFrame({'missing %': data.isnull().sum()*100/len(data)})
missingValueFeatures[missingValueFeatures['missing %']>0]


# In[34]:


# Impute TotalCharges as per Tenure Column
print('Before Imputation')
print(data['TotalCharges'].describe())
data.sort_values('tenure', inplace=True)
# use back fill to replace nan values
data['TotalCharges'].fillna(method='bfill', inplace=True)
print('\nAfter Imputation')
print(data['TotalCharges'].describe())


# ## Handling Categorical Features (Label Encoding & One Hot Encoding)

# In[38]:


df = data.copy()
path = '/home/aarush100616/Downloads/Projects/Telecom Users Recall Analysis'
for i, feature in enumerate(categorical_features):
    le = LabelEncoder()
    ohe = OneHotEncoder(sparse=False)
    # create directory to save label encoding models
    if not os.path.exists(os.path.join(path, "TextEncoding")):
        os.makedirs(os.path.join(path, "TextEncoding"))
    # perform label encoding
    le.fit(df[feature])
    # save the encoder
    joblib.dump(le, open(os.path.join(path, "TextEncoding/le_{}.sav".format(feature)), 'wb'))
    # transfrom training data
    df[feature] = le.transform(df[feature])
    # get classes & remove first column to elude from dummy variable trap
    columns = list(map(lambda x: feature+' '+str(x), list(le.classes_)))[1:]
    # save classes
    joblib.dump(columns, 
                open(os.path.join(path, "TextEncoding/le_{}_classes.sav".format(feature)), 'wb'))
    # load classes
    columns = joblib.load(
        open(os.path.join(path, "TextEncoding/le_{}_classes.sav".format(feature)), 'rb'))
    if len(le.classes_)>2:
        # perform one hot encoding
        ohe.fit(df[[feature]])
        # save the encoder
        joblib.dump(ohe, 
                    open(os.path.join(path, "TextEncoding/ohe_{}.sav".format(feature)), 'wb'))
        # transfrom training data
        # removing first column of encoded data to elude from dummy variable trap
        tempData = ohe.transform(df[[feature]])[:, 1:]
        # create Dataframe with columns as classes
        tempData = pd.DataFrame(tempData, columns=columns)
    else:
        tempData = df[[feature]]
        # create dataframe with all the label encoded categorical features along with hot encoding
    if i==0:
        encodedData = pd.DataFrame(data=tempData, columns=tempData.columns.values.tolist())
    else:
        encodedData = pd.concat([encodedData, tempData], axis=1)


# In[39]:


# merge numerical features and categorical encoded features
df = df[numerical_features+['Churn']]
df = pd.concat([df, encodedData], axis=1)
df.info()


# ## Training Model

# In[43]:


from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn import metrics, preprocessing
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.svm import SVC


# In[44]:


train_data = df.copy()
feature_cols = [feature for feature in train_data.columns if feature not in(['Churn'])]
''' Rescaling to [0,1] '''
scaler = MinMaxScaler()
scaler.fit(train_data[feature_cols])
train_data[feature_cols] = scaler.transform(train_data[feature_cols])


# In[45]:


X = train_data[feature_cols]
y = train_data['Churn'].map({'No':0, 'Yes':1})
validation_size = 0.25
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=validation_size, 
                                                    random_state=4, stratify=y)


# ### Model 1: Logistic Regression

# In[46]:


model = LogisticRegression(class_weight={0:1, 1:10})
model.fit(X_train, y_train)


# In[47]:


y_pred = model.predict(X_train)
print('Train metrics...')
print(confusion_matrix(y_train, y_pred))
print(classification_report(y_train, y_pred))
y_pred = model.predict(X_test)
print('Validation metrics...')
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))


# In[48]:


''' metrics on original data '''
y_pred = model.predict(train_data[feature_cols])
def make_cm(matrix, columns):
    n = len(columns)
    act = ['actual Churn'] * n
    pred = ['prediction Churn'] * n
    cm = pd.DataFrame(matrix, 
        columns=[pred, columns], index=[act, columns])
    return cm
df_matrix=make_cm(
    confusion_matrix(y, y_pred),['No','Yes'])
display(df_matrix)
print(classification_report(y, y_pred))


# ### Model 2: SVM

# In[49]:


model = SVC(class_weight={0: 1, 1: 10})
model.fit(X_train, y_train)


# In[50]:


y_pred = model.predict(X_train)
print('Train metrics...')
print(confusion_matrix(y_train, y_pred))
print(classification_report(y_train, y_pred))
y_pred = model.predict(X_test)
print('Test metrics...')
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))


# In[51]:


''' metrics on original data '''
y_pred = model.predict(train_data[feature_cols])
def make_cm(matrix, columns):
    n = len(columns)
    act = ['actual Churn'] * n
    pred = ['prediction Churn'] * n
    cm = pd.DataFrame(matrix, 
        columns=[pred, columns], index=[act, columns])
    return cm
df_matrix=make_cm(
    confusion_matrix(y, y_pred),['No','Yes'])
display(df_matrix)
print(classification_report(y, y_pred))

